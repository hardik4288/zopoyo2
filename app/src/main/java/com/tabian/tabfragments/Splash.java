package com.tabian.tabfragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;


public class Splash extends AppCompatActivity {
    private final int SPLASH_DISPLAY_LENGTH = 2000;
    SharedPreferences sharedPreferences;
    Boolean firstTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {

                if (!firstTime){
                    SharedPreferences.Editor editor = sharedPreferences.edit();

                    editor.putBoolean("firstTime",firstTime);
                    editor.apply();

                    Intent i  = new Intent(Splash.this,MainActivity.class);
                    startActivity(i);
                    finish();

                }
                else if (firstTime){
                    SharedPreferences.Editor editor = sharedPreferences.edit();

                    Intent i  = new Intent(Splash.this,MainActivity.class);
                    editor.putBoolean("firstTime",firstTime);
                    editor.apply();
                    startActivity(i);
                    finish();
                }
            }
        }, SPLASH_DISPLAY_LENGTH);


        sharedPreferences = getSharedPreferences("MyPrefs",MODE_PRIVATE);

        firstTime = sharedPreferences.getBoolean("firstTime",true);




    }
}



