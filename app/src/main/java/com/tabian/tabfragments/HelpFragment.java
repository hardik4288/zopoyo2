package com.tabian.tabfragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;


public class HelpFragment extends AppCompatActivity {
    ImageView facebook,instagram,pintrust,twitter;
    LinearLayout help,privacy,contact,partner;

    public HelpFragment() {
        // Required empty public constructor
    }



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_help);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        facebook = findViewById(R.id.facebook);
        instagram =findViewById(R.id.insta);
        pintrust = findViewById(R.id.pintrest);
        twitter = findViewById(R.id.twitter);
        help =findViewById(R.id.terms);
        privacy =findViewById(R.id.privacy);
        contact =findViewById(R.id.contact);




        privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Uri uri = Uri.parse("http://zopoyo.in/privacy-policy/");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);




            }
        });
        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(HelpFragment.this,WebActivity.class);
                startActivity(intent);




            }
        });
       help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Uri uri = Uri.parse("http://zopoyo.in/about-us/");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);




            }
        });
        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Uri uri = Uri.parse("http://www.facebook.com/zopoyoin");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);




            }
        });
        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Uri uri = Uri.parse("http://www.twitter.com/zopoyoin");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);




            }
        });
        pintrust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Uri uri = Uri.parse("http://www.in.pinterest.com/zopoyo");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);




            }
        });
        instagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Uri uri = Uri.parse("http://www.instagram.com/zopoyo");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);




            }
        });
    }


}
