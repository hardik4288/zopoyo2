package com.tabian.tabfragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


public class Feedback extends AppCompatActivity {
 boolean check = false;
 Button feeddialog;
TextView perform,burger,coupon,others,suggest,design;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_feedback);
        feeddialog = findViewById(R.id.dmgbtn);
         perform = findViewById(R.id.appper);
        burger = findViewById(R.id.burger);
        coupon = findViewById(R.id.design);
        others = findViewById(R.id.other);
        suggest = findViewById(R.id.suggest);
        design = findViewById(R.id.coupons);

        perform.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               perform.setBackground(getDrawable(R.drawable.changecolour));


            }
        });
        burger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                burger.setBackground(getDrawable(R.drawable.changecolour));
            }
        });
        coupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                coupon.setBackground(getDrawable(R.drawable.changecolour));
            }
        });
        others.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                others.setBackground(getDrawable(R.drawable.changecolour));
            }
        });
        suggest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suggest.setBackground(getDrawable(R.drawable.changecolour));
            }
        });
        design.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                design.setBackground(getDrawable(R.drawable.changecolour));
            }
        });





        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      feeddialog.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              AlertDialog.Builder mBuilder = new AlertDialog.Builder(Feedback.this);
              final View mView = getLayoutInflater().inflate(R.layout.feed_dialog,null);
      //        Button back = mView.findViewById(R.id.back);
              Button submit = mView.findViewById(R.id.submit);
              mBuilder.setView(mView);
              AlertDialog dialog = mBuilder.create();
              dialog.show();

              submit.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                      AlertDialog.Builder mBuilder = new AlertDialog.Builder(Feedback.this);
                      final View mView = getLayoutInflater().inflate(R.layout.dialog_ok,null);
                      Button submit = mView.findViewById(R.id.btnok);

                      mBuilder.setView(mView);
                      AlertDialog dialog = mBuilder.create();
                      dialog.show();
                      submit.setOnClickListener(new View.OnClickListener() {
                          @Override
                          public void onClick(View v) {
                              Intent intent = new Intent(Feedback.this,MainActivity.class);
                              startActivity(intent);

                          }
                      });




                  }
              });
          }
      });


    }

    }

