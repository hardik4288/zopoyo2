package com.tabian.tabfragments;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class VerticalAdapter extends RecyclerView.Adapter<VerticalAdapter.ProductViewHolder> {
    private Context mCtx;
    SingleVertical singleVertical = new SingleVertical();
    public static final String  ID ="id";
    public static final String  SID ="sid";
    public static final String  NAME ="store_title";
    public static final String  DESC ="meta_title";
    public static final String IMG = "store_image";
    private List<SingleVertical> productList;
    class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView textViewTitle, offers;
        List<SingleVertical> productList = new ArrayList<>();
        ImageView companyview;
        CardView card_company;
        private Context mctx;

        public ProductViewHolder(View itemView,Context mctx,List<SingleVertical>productList) {
            super(itemView);
            this.mctx = mctx;
            this.productList = productList;

            card_company = itemView.findViewById(R.id.card_comapany);
            companyview = itemView.findViewById(R.id.companyimg);

            textViewTitle = itemView.findViewById(R.id.textViewTitle);
            itemView.setClickable(true);
            itemView.setOnClickListener(this);

        }
        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            SingleVertical singleVertical = this.productList.get(position);
            Intent intent = new Intent(this.mctx, Deals.class);
            intent.putExtra(ID, singleVertical.getId());
            intent.putExtra(SID,singleVertical.getId());
            intent.putExtra(DESC,singleVertical.getdesc());
            intent.putExtra(NAME, singleVertical.getTitle());
            intent.putExtra(IMG, singleVertical.getImg());

            this.mctx.startActivity(intent);

        }
    }
    public VerticalAdapter(Context mCtx, List<SingleVertical> productList) {

        this.mCtx = mCtx;
        this.productList = productList;
    }
    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.vertical_single_row, null);
        return new ProductViewHolder(view,view.getContext(),productList);

    }
    public List<SingleVertical> getProductList() {
        return productList;
    }
    @Override
    public void onBindViewHolder(ProductViewHolder holder, final int position) {


       SingleVertical product = productList.get(position);
       holder.textViewTitle.setText(productList.get(position).getTitle());
      Picasso.with(mCtx).load("http://zopoyo.in/" + product.getImg()).fit().centerInside().into(holder.companyview);


    }

        @Override
        public int getItemCount () {
            return productList.size();
        }



}