package com.tabian.tabfragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class HiddenAdapter extends RecyclerView.Adapter<HiddenAdapter.ProductViewHolder> {
    public static final String  CNAME = "store_title";
    public static final String  CDESC ="coupon_title";
    public static final String  CLONGDESC ="coupon_description";
    public static final String  CCOUPONCODE ="coupon_code";
    public static final String  CLLINK ="buynowurl";
    public static final String  CID ="id";
    public static final String  CIMG = "store_images";
    public static final String  HIMG = "store_image";




    private Context mCtx;


    private List<hidden> productList;
    RequestOptions options ;
    class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewTitle,percent,expire,desc;
        Context mCtx;
        ImageView img;
        List<hidden>productList = new ArrayList<>();
        RelativeLayout l1;
        public ProductViewHolder(View itemView,Context mCtx,List<hidden>productList ) {
            super(itemView);
            this.mCtx =mCtx;
            this.productList = productList;
            desc = itemView.findViewById(R.id.desc);
            img = itemView.findViewById(R.id.img_view);
            expire = itemView.findViewById(R.id.expire);
            percent = itemView.findViewById(R.id.percent);
            textViewTitle = itemView.findViewById(R.id.textViewTitle);
            l1 = itemView.findViewById(R.id.l1);
            itemView.setClickable(true);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
           hidden hidden1 = this.productList.get(position);
            Intent intent =  new Intent(mCtx, CoupionActivity.class);
            intent.putExtra(CID,hidden1.getId());
            intent.putExtra(CNAME,hidden1.getName());
            intent.putExtra(CDESC,hidden1.getTitle());
            intent.putExtra(CLONGDESC,hidden1.getLongdesc());
            intent.putExtra(CCOUPONCODE,hidden1.getCouponcode());
            intent.putExtra(CLLINK,hidden1.getUrl());
            intent.putExtra(CIMG,hidden1.getImg());
            intent.putExtra(HIMG,hidden1.getCouponimage());

            this.mCtx.startActivity(intent);

        }
    }

    //getting the context and product list with constructor
    public HiddenAdapter(Context mCtx, List<hidden> productList) {
        this.mCtx = mCtx;
        this.productList = productList;
        hidden hidden = new hidden();
        options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.amazon)
                .error(R.drawable.jabong);
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);

        View view = inflater.inflate(R.layout.hidden, null);
        return new ProductViewHolder(view,view.getContext(),productList);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
       hidden hidden = productList.get(position);

       String imgurl = hidden.getImg();
       String imgur = hidden.getCouponimage();



        hidden product = productList.get(position);
        holder.expire.setText(productList.get(position).getExpire());
        holder.percent.setText(productList.get(position).getPercent());
        holder.textViewTitle.setText(productList.get(position).getTitle());
       if(imgur != null){
        Picasso.with(mCtx).load("http://zopoyo.in/" + imgur).fit().centerInside().into(holder.img);}
       else if(imgurl != null) {
            Picasso.with(mCtx).load("http://zopoyo.in/" + imgurl).fit().centerInside().into(holder.img);
        }

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }
}