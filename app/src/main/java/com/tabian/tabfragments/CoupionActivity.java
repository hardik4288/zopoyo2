package com.tabian.tabfragments;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.tabian.tabfragments.HiddenAdapter.CDESC;
import static com.tabian.tabfragments.HiddenAdapter.CID;
import static com.tabian.tabfragments.HiddenAdapter.CIMG;
import static com.tabian.tabfragments.HiddenAdapter.CLLINK;
import static com.tabian.tabfragments.HiddenAdapter.CLONGDESC;
import static com.tabian.tabfragments.HiddenAdapter.CNAME;
import static com.tabian.tabfragments.HiddenAdapter.HIMG;


public class CoupionActivity extends AppCompatActivity {
    Button couponbutton, backbtn;
    protected boolean _active = true;
    List<coupon> productList = new ArrayList<>();
    private String url;
    TextView shotdesc, longsdesc, dsccpn;
    ImageView backButton;

    private String URL_JSON = "http://zopoyo.in/webapp/coupons/";
    private TextView textView;
    private String refer_link;
    String store_id;

    String images, himage, getImages, gettimage;

    private JsonArrayRequest ArrayRequest;
    private RequestQueue requestQueue;
    private ImageView image;
    private Button webbtn;
    TextView companyname, description;
    private final int SPLASH_DISPLAY_LENGTH = 3000;
    ClipboardManager clipboard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupion);
        webbtn = findViewById(R.id.webcpbtn);

        couponbutton = (Button) findViewById(R.id.cpbutton);
        backbtn = findViewById(R.id.bkbtn);
        companyname = findViewById(R.id.textView);
        description = findViewById(R.id.textView3);
        backButton = findViewById(R.id.backButton);
        longsdesc = findViewById(R.id.textView2);
        textView = findViewById(R.id.backtext);
        isNetworkConnectionAvailable();


        final Intent intent = getIntent();
        String id = intent.getStringExtra(CID);
        refer_link = intent.getStringExtra(CLLINK);
        himage = intent.getStringExtra(HIMG);
        images = intent.getStringExtra(CIMG);
        getImages = "http://www.zopoyo.in/" + images;
        // gettimage = "http://www.zopoyo.in/"

        //Log.d("showImg",images);


        final String name = intent.getStringExtra(CNAME);
        companyname.setText(name);
        textView.setText(name);

        String longdesc = intent.getStringExtra(CLONGDESC);
        longsdesc.setText(longdesc);
        String desc = intent.getStringExtra(CDESC);

        image = findViewById(R.id.imageView);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });



        if (images != null) {

            Glide.with(this)
                    .load(getImages)
                    .into(image);

        } else if (himage != null) {
            Glide.with(this)
                    .load("http://www.zopoyo.in/" + himage)
                    .into(image);
        }
        backbtn.setText("View more Coupon from " + name);

        description.setText(desc);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                //  Intent intent = new Intent(CoupionActivity.this,Deals.class);

                // startActivity(intent);
            }
        });
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        couponbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(CoupionActivity.this);
                View mView = getLayoutInflater().inflate(R.layout.dialog_coupion_layout, null);

                TextView offercpn = mView.findViewById(R.id.offer_txt);
                dsccpn = mView.findViewById(R.id.desc_cpn);

                mBuilder.setView(mView);

                AlertDialog dialog = mBuilder.create();
                dialog.show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                        Uri uri = Uri.parse(refer_link);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                        ClipData clip = ClipData.newPlainText("label", "Text to copy");
                        clipboard.setPrimaryClip(clip);
                    }
                }, SPLASH_DISPLAY_LENGTH);
                final Intent intent = getIntent();
                String names = intent.getStringExtra(CNAME);
                Toast.makeText(CoupionActivity.this, "Coupon Code Copied", Toast.LENGTH_SHORT).show();
                Toast.makeText(CoupionActivity.this, "Redirecting to " + names, Toast.LENGTH_SHORT).show();

                Button btnwebview = mView.findViewById(R.id.webcpbtn);
                btnwebview.setText("Vist to "+name);
                btnwebview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Uri uri = Uri.parse(refer_link);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                });
            }
        });
    }

    public void checkNetworkConnection() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("No internet Connection");
        builder.setMessage("Please turn on internet connection to continue");
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public boolean isNetworkConnectionAvailable() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
        if (isConnected) {
            Log.d("Network", "Connected");
            return true;
        } else {
            checkNetworkConnection();
            Log.d("Network", "Not Connected");
            return false;
        }
    }

}
