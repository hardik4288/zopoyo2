package com.tabian.tabfragments;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by hardik on 20/12/18.
 */

public class SliderActivity extends AppCompatActivity {
    private ViewPager viewPager;
    private SliderAdapter myadapter;
    Button nxtbtn,bckbtn;
    private int CurrentPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_screen);


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        myadapter = new SliderAdapter(this);
        viewPager.setAdapter(myadapter);
        nxtbtn = findViewById(R.id.button1);
        bckbtn = findViewById(R.id.button2);

        nxtbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(CurrentPage + 3);


            }
        });
       bckbtn.setVisibility(View.INVISIBLE);



    }
    ViewPager.OnPageChangeListener viewListner = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {

        }

        @Override
        public void onPageSelected(int i) {
            CurrentPage = i;

        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };
}
