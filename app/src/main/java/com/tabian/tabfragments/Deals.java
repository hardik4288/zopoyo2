package com.tabian.tabfragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.tabian.tabfragments.HorizontalAdapter.HID;
import static com.tabian.tabfragments.HorizontalAdapter.HIMG;
import static com.tabian.tabfragments.HorizontalAdapter.HNAME;
import static com.tabian.tabfragments.HorizontalAdapter.Hdesc;
import static com.tabian.tabfragments.VerticalAdapter.DESC;
import static com.tabian.tabfragments.VerticalAdapter.ID;
import static com.tabian.tabfragments.VerticalAdapter.IMG;
import static com.tabian.tabfragments.VerticalAdapter.NAME;


public class Deals extends AppCompatActivity {

    List<hidden> productList = new ArrayList<>();
  //  private String URL_JSON = "http://zopoyo.in/webapp/coupons/";
    private String URL = "http://zopoyo.in/webapp/stors_ids/";

    String image;
    String Name,desc;
    String url;
    ImageView backbutn;
    String sid;
    String descs;
    TextView couponcount;



    String store_id;
    TextView companyname;
    private String id;
    TextView description;
    ImageView imageView;


    private JsonArrayRequest ArrayRequest;
    private RequestQueue requestQueue;
    String storeId, store, name;

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_deals);

        recyclerView = (RecyclerView) findViewById(R.id.company_recyler);
       companyname = findViewById(R.id.company_name);
     description = findViewById(R.id.desc);
        Intent intent = getIntent();
        id = intent.getStringExtra(ID);

      name = intent.getStringExtra(NAME);
        descs = intent.getStringExtra(DESC);
        couponcount = findViewById(R.id.couponcount);
        imageView = findViewById(R.id.company_view);
        store_id = intent.getStringExtra(HID);
        store = intent.getStringExtra(HIMG);
        Name = intent.getStringExtra(HNAME);
        desc = intent.getStringExtra(Hdesc);
        sid = intent.getStringExtra(sid);
        backbutn = findViewById(R.id.backButtons);

        if(name != null){
            companyname.setText(name);
        }else if(Name != null){
            companyname.setText(Name);
        }
        if(descs != null){
            description.setText(descs);
        }else if(desc != null){
            description.setText(desc);
        }



        image = intent.getStringExtra(IMG);
     if(image != null)

{
        Glide.with(this)
                .load("http://www.zopoyo.in/" + image)
               .into(imageView);}
               else if(store_id != null){
         Glide.with(this)
                 .load("http://www.zopoyo.in/" + store_id)
                 .into(imageView);

     }
        hidden Hidden = new hidden();
        Hidden.setName(name);


        if(id != null){
            url = id;
        }else if(store_id != null){
            url = store_id;
        }else if(sid != null){
            url = sid;
        }
        jsoncall();
        backbutn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                //  Intent intent = new Intent(CoupionActivity.this,Deals.class);

                // startActivity(intent);
            }
        });
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      isNetworkConnectionAvailable();
    }
    public void jsoncall() {
        if(id != null)
        ArrayRequest = new JsonArrayRequest(URL+url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d("responseJSON", String.valueOf(response));

                JSONObject jsonObject = null;


                for (int i = 0; i < response.length(); i++) {


                    try {

                        jsonObject = response.getJSONObject(i);
                        hidden anime = new hidden();
                        anime.setTitle(jsonObject.getString("coupon_title"));
                        anime.setPercent(jsonObject.getString("percentage_url"));
                        anime.setLongdesc(jsonObject.getString("coupon_description"));
                        anime.setExpire(jsonObject.getString("expiry_date"));
                        anime.setCouponcode(jsonObject.getString("coupon_code"));
                        anime.setUrl(jsonObject.getString("buynowurl"));


                        anime.setImg(image);
                        anime.setName(name);
                        productList.add(anime);
                        couponcount.setText(String.valueOf(response.length())+" Coupons");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                setRvadapter(productList);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
            try {
                Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                if (cacheEntry == null) {
                    cacheEntry = new Cache.Entry();
                }
                final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
                final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                long now = System.currentTimeMillis();
                final long softExpire = now + cacheHitButRefreshed;
                final long ttl = now + cacheExpired;
                cacheEntry.data = response.data;
                cacheEntry.softTtl = softExpire;
                cacheEntry.ttl = ttl;
                String headerValue;
                headerValue = response.headers.get("Date");
                if (headerValue != null) {
                    cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                }
                headerValue = response.headers.get("Last-Modified");
                if (headerValue != null) {
                    cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                }
                cacheEntry.responseHeaders = response.headers;
                final String jsonString = new String(response.data,
                        HttpHeaderParser.parseCharset(response.headers));
                return Response.success(new JSONArray(jsonString), cacheEntry);
            } catch (UnsupportedEncodingException | JSONException e) {
                return Response.error(new ParseError(e));
            }
        }

            @Override
            protected void deliverResponse(JSONArray response) {
            super.deliverResponse(response);
        }

            @Override
            public void deliverError(VolleyError error) {
            super.deliverError(error);
        }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }
        };


        requestQueue = Volley.newRequestQueue(Deals.this);
        requestQueue.add(ArrayRequest);
    }

    public void setRvadapter(List<hidden> productList) {

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(Deals.this));
        HiddenAdapter adapter = new HiddenAdapter(Deals.this, productList);
        recyclerView.setAdapter(adapter);


    }
    public void checkNetworkConnection(){
        AlertDialog.Builder builder =new AlertDialog.Builder(this);
        builder.setTitle("No internet Connection");
        builder.setMessage("Please turn on internet connection to continue");
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public boolean isNetworkConnectionAvailable(){
        ConnectivityManager cm =
                (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
        if(isConnected) {
            Log.d("Network", "Connected");
            return true;
        }
        else{
            checkNetworkConnection();
            Log.d("Network","Not Connected");
            return false;
        }
    }


}