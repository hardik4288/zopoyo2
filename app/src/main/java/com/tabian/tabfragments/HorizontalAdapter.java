package com.tabian.tabfragments;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;



public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.ProductViewHolder> {
    SingleHorizontal singleHorizontal;
    public static final String  HID ="id";
    public static final String  HIMG ="store_image";
    public static final String  HNAME ="store_title";
    public static final String  Hdesc ="meta_title";


    String url =" http://www.zopoyo.in/";
    String str;


    //this context we will use to inflate the layout
    private Context mCtx;
    RequestOptions options ;

    //we are storing all the products in a list
    private List<SingleHorizontal> objects;
    class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView textViewTitle, offers;
        ImageView img;
        List<SingleHorizontal>objects = new ArrayList<>();
        Context mCtx;


        public ProductViewHolder(View itemView,Context mCtx,List<SingleHorizontal>objects) {
            super(itemView);
            this.mCtx =mCtx;
            this.objects = objects;

            img = itemView.findViewById(R.id.image_view);

            itemView.setClickable(true);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
                  Intent intent;
                    int position = getAdapterPosition();
                    SingleHorizontal hidden1 = this.objects.get(position);
                    intent = new Intent(mCtx, Deals.class);
                    intent.putExtra(HID,hidden1.getId());
                    intent.putExtra(HIMG,hidden1.getImage());
                    intent.putExtra(HNAME,hidden1.getName());
                    intent.putExtra(Hdesc,hidden1.getShortdesc());

                    mCtx.startActivity(intent);
            }
        }
    //getting the context and product list with constructor
    public HorizontalAdapter(Context mCtx, ArrayList<SingleHorizontal> objects) {
        this.mCtx = mCtx;
        this.objects = objects;
        options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.cart)
                .error(R.drawable.cart);
    }
    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.horizontal_single_row, null);
        return new ProductViewHolder(view,view.getContext(),objects);

    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        //getting the product of the specified position

        SingleHorizontal singleHorizontal = objects.get(position);
        str = singleHorizontal.getImage();
        Glide.with(mCtx).load("http://zopoyo.in/"+str).apply(options).into(holder.img);
    }
    @Override
    public int getItemCount () {
        return objects.size();
    }
public void updatelist(List<SingleHorizontal> newList)
{
    objects = new ArrayList<>();
    objects.addAll(newList);
    notifyDataSetChanged();
}
}