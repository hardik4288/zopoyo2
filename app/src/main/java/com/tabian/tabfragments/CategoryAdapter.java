package com.tabian.tabfragments;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hardik on 26/12/18.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ProductViewHolder> {
    private Context mCtx;
    public static final String  CATEID ="id";
   private RequestOptions options ;

    //we are storing all the products in a list
    private List<SingleVertical> productList;
    class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView textViewTitle, offers;
        CardView card_company;
        List<SingleVertical> productList = new ArrayList<>();
        private Context mctx;


        public ProductViewHolder(View itemView,Context mctx,List<SingleVertical>productList) {
            super(itemView);
            this.mctx = mctx;
            this.productList = productList;

            card_company = itemView.findViewById(R.id.card_comapany);
            textViewTitle = itemView.findViewById(R.id.textViewTitle);
            itemView.setClickable(true);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
           Intent intent;
            int position = getAdapterPosition();
            SingleVertical singleVertical = this.productList.get(position);
            intent =  new Intent(mCtx,AllCompany.class);
            intent.putExtra(CATEID, singleVertical.getCategoryid());
            mCtx.startActivity(intent);
        }

    }
    //getting the context and product list with constructor
    public CategoryAdapter(Context mCtx, List<SingleVertical> productList) {
        this.mCtx = mCtx;
        this.productList = productList;


    }

    @Override
    public CategoryAdapter.ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.vertical_single_row, null);

        return new CategoryAdapter.ProductViewHolder(view,view.getContext(),productList);
    }
    @Override
    public void onBindViewHolder(CategoryAdapter.ProductViewHolder holder, int position) {
        //getting the product of the specified position
        final SingleVertical product = productList.get(position);
        holder.textViewTitle.setText(productList.get(position).getTitle());
    }

    @Override
    public int getItemCount () {
        return productList.size();
    }




}
